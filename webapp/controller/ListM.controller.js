sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/NavContainer",
	"sap/m/MessageToast"
], function(Controller,MessageToast) {
	"use strict";

	return Controller.extend("NT20171114.controller.ListM", {
		onInit: function() {

			this.router = sap.ui.core.UIComponent.getRouterFor(this);

			var oModel2 = new sap.ui.model.json.JSONModel("/FM7API/api/dashboard/processing");
			this.getView().setModel(oModel2, "FM7");
		},
		onItemPress: function(oEvent) {
			// this.router.navTo("ListD");

			var oObject = oEvent.getSource().getBindingContext("FM7");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			if (oItem.DiagramName === "費用課付款申請書") {
console.log('IN' + oItem.RequisitionId);

				this.router.navTo("ListD", { RequisitionId: oItem.RequisitionId });
			} else {
				console.log(oItem.DiagramName);
			}
		}

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf NT20171114.view.ListM
		 */
		//	onInit: function() {
		//
		//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf NT20171114.view.ListM
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf NT20171114.view.ListM
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf NT20171114.view.ListM
		 */
		//	onExit: function() {
		//
		//	}

	});

});