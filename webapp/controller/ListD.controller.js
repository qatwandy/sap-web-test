sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("NT20171114.controller.ListD", {
		onInit: function() {
			// var oModelA = new sap.ui.model.json.JSONModel("/FM7API/api/fdp/m/7899398f-6dc1-4fbd-9d8f-ffc642b8bcd9");
			// this.getView().setModel(oModelA, "FM7B");

			this.router = sap.ui.core.UIComponent.getRouterFor(this);
			this.router.getRoute("ListD").attachMatched(this.handleRouteMatched, this);

		},
		handleRouteMatched: function(oEvent) {
			var sRequisitionId = oEvent.getParameter("arguments").RequisitionId;
console.log('handleRouteMatchedRId:' + sRequisitionId);
			 var oModel = new sap.ui.model.json.JSONModel("/FM7API/api/fdp/m/" + sRequisitionId);
			 this.getView().setModel(oModel, "FM7B");
		}

	});

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf NT20171114.view.ListD
	 */
	//	onInit: function() {
	//
	//	},

	/**
	 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
	 * (NOT before the first rendering! onInit() is used for that one!).
	 * @memberOf NT20171114.view.ListD
	 */
	//	onBeforeRendering: function() {
	//
	//	},

	/**
	 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
	 * This hook is the same one that SAPUI5 controls get after being rendered.
	 * @memberOf NT20171114.view.ListD
	 */
	//	onAfterRendering: function() {
	//
	//	},

	/**
	 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
	 * @memberOf NT20171114.view.ListD
	 */
	//	onExit: function() {
	//
	//	}
});